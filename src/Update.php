<?php

namespace judahnator\TimeTracker;

use Composer\Script\Event;

class Update
{

    // Before updating we need to save the options file
    public static function preUpdate(Event $event): void
    {
        $event->getIO()->write('Backing up your time tracking settings and timers to your system temp dir.');
        copy(__DIR__.'/../options.json', sys_get_temp_dir().'/timetracker-options-backup.json');
    }

    // After updating we need to check to see if we have a backup options file to restore
    public static function postUpdate(Event $event): void
    {
        if (file_exists(sys_get_temp_dir().'/timetracker-options-backup.json')) {
            $event->getIO()->write('Restoring backed up time tracking settings and timers.');
            copy(sys_get_temp_dir().'/timetracker-options-backup.json', __DIR__.'/../options.json');
            unlink(sys_get_temp_dir().'/timetracker-options-backup.json');
        }
    }
}
