<?php

namespace judahnator\TimeTracker\Commands;

use Carbon\Carbon;
use function judahnator\TimeTracker\log_location;
use function judahnator\TimeTracker\timers;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

class StopTimerCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('timer:stop')
            ->setDescription('Stops a given timer and optionally adds it to the log.')
            ->addArgument('timer id', InputArgument::OPTIONAL, 'The timer ID to stop.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $timers = timers();
        if (empty($timers)) {
            $output->writeln('<error>There are no timers to stop</error>');
            return;
        }

        $timer_id = $input->getArgument('timer id');

        /** @var QuestionHelper $question_helper */
        $question_helper = $this->getHelper('question');

        if (!$timer_id) {
            if (count($timers) === 1) {
                // Only one timer running, the user obviously wants that one
                $timer_id = key($timers);
                $output->writeln("Only one timer running, selecting {$timer_id} automatically.");
            } else {
                // There are more than one timers, ask which one we are stopping
                $timer_id = $question_helper->ask(
                    $input,
                    $output,
                    new ChoiceQuestion(
                        'Select the timer you wish to stop.',
                        array_map(
                            function (array $timer): string {
                                return ($timer['description'] ?: 'No description...') . ' [' . Carbon::createFromTimestamp($timer['start'])->diffForHumans(null, true) . ']';
                            },
                            $timers
                        )
                    )
                );
            }
        }

        if (!array_key_exists($timer_id, $timers)) {
            $output->writeln("<error>The timer {$timer_id} is invalid.</error>");
        }

        $timer = $timers[$timer_id];

        $timer['client'] = $question_helper->ask(
            $input,
            $output,
            new Question("Client: ({$timer['client']})" . PHP_EOL, $timer['client'])
        );

        $timer['description'] = $question_helper->ask(
            $input,
            $output,
            new Question("Description: ({$timer['description']})" . PHP_EOL, $timer['description'])
        );

        if ($question_helper->ask($input, $output, new ConfirmationQuestion('Add this time to the log? (Y/n)' . PHP_EOL))) {
            $handle = fopen(log_location(), 'a');
            fputcsv($handle, [
                $timer_id,
                Carbon::createFromTimestamp($timer['start'])->diffForHumans(null, true, false, 2),
                Carbon::createFromTimestamp($timer['start'])->format('m/d/Y H:i:s'),
                Carbon::now()->format('m/d/Y H:i:s'),
                $timer['description'] ?: '',
                $timer['client'] ?: ''
            ]);
            fclose($handle);
        }

        if ($question_helper->ask($input, $output, new ConfirmationQuestion('Remove from current timers? (Y/n)' . PHP_EOL))) {
            unset($timers[$timer_id]);
            timers($timers);
        }
    }
}
