<?php

namespace judahnator\TimeTracker\Commands;

use function judahnator\TimeTracker\timers;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class StartTimerCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('timer:start')
            ->setDescription('Starts a timer')
            ->addOption('client', 'c', InputOption::VALUE_OPTIONAL)
            ->addArgument('description', InputArgument::OPTIONAL, 'A brief description of what you are doing');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $timer_id = uniqid();
        $timers = timers();
        $description = $input->getArgument('description');
        $timers[$timer_id] = [
            'client' => $input->getOption('client'),
            'description' => $description,
            'start' => time()
        ];
        timers($timers);

        $message = "Timer {$timer_id} has started";
        $message .= $description ? " with description: \"{$description}\"." : " with no description.";
        $output->writeln($message);
    }
}
