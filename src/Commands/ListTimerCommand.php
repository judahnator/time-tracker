<?php

namespace judahnator\TimeTracker\Commands;

use Carbon\Carbon;
use function judahnator\TimeTracker\timers;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ListTimerCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('timer:list')
            ->setDescription('List the currently running timers.')
            ->addOption('client', 'c', InputOption::VALUE_OPTIONAL);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $client = $input->getOption('client');
        $timers = array_filter(
            timers(),
            function (array $timer) use ($client): bool {
                return (!$client || $client === $timer['client']);
            }
        );

        $table = new Table($output);
        $table->setHeaders(['id', 'time', 'client', 'description']);
        $table->addRows(
            array_map(
                function (array $timer, string $id): array {
                    return [
                        $id,
                        Carbon::createFromTimestamp($timer['start'])->diffForHumans(null, true, false, 2),
                        $timer['client'],
                        $timer['description']
                    ];
                },
                $timers,
                array_keys($timers)
            )
        );

        $table->render();
    }
}
