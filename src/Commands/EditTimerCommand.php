<?php

namespace judahnator\TimeTracker\Commands;

use function judahnator\TimeTracker\timers;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

class EditTimerCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('timer:edit')
            ->setDescription('Edits a timer')
            ->addArgument('timer', InputArgument::OPTIONAL, 'The timer to edit')
            ->addOption('client', 'c', InputOption::VALUE_OPTIONAL, 'Overwrites the client')
            ->addOption('description', 'd', InputOption::VALUE_OPTIONAL, 'What to set the timer description to');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $timers = timers();
        if (empty($timers)) {
            $output->writeln('<error>There are no timers to edit</error>');
            return;
        }

        $timer_id = $input->getArgument('timer');
        $client = $input->getOption('client');
        $description = $input->getOption('description');


        /** @var QuestionHelper $question_helper */
        $question_helper = $this->getHelper('question');

        if (!$timer_id || !array_key_exists($timer_id, $timers)) {
            if (count($timers) === 1) {
                // The user likely wants to edit the only existing timer
                $timer_id = key($timers);
                $output->writeln("Only one timer running, selecting {$timer_id} automatically");
            } else {
                $timer_id = $question_helper->ask(
                    $input,
                    $output,
                    new ChoiceQuestion(
                        'Select the timer you wish to edit'.PHP_EOL,
                        array_map(
                            function (array $timer): string {
                                return $timer['description'] ?: '';
                            },
                            $timers
                        )
                    )
                );
            }
        }

        $timer = &$timers[$timer_id];

        if (!$client) {
            $client = $question_helper->ask(
                $input,
                $output,
                new Question("Client: ({$timer['client']})".PHP_EOL, $timer['client'])
            );
        }

        if (!$description) {
            $description = $question_helper->ask(
                $input,
                $output,
                new Question("Description: ({$timer['description']})".PHP_EOL, $timer['description'])
            );
        }

        $timer['client'] = $client;
        $timer['description'] = $description;

        timers($timers);

        $output->writeln("<info>Timer {$timer_id} has been updated.</info>");
    }
}
