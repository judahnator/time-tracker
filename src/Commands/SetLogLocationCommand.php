<?php

namespace judahnator\TimeTracker\Commands;

use function judahnator\TimeTracker\log_location;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetLogLocationCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('set:log')
            ->setDescription('Sets the CSV time log location')
            ->addArgument('path', InputArgument::REQUIRED, 'The full path to the log file to use.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        log_location($input->getArgument('path'));
        if (!file_exists(log_location())) {
            $handle = fopen(log_location(), 'w');
            fputcsv($handle, [
                'id',
                'time',
                'start',
                'end',
                'description',
                'client'
            ]);
            fclose($handle);
        }
        $output->writeln('<info>The time log location has been updated to '.log_location().'</info>');
    }
}
