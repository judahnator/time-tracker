<?php

namespace judahnator\TimeTracker\Commands;

use function judahnator\TimeTracker\timers;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class DeleteTimerCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('timer:delete')
            ->setDescription('Deletes a given timer')
            ->addArgument('timer', InputArgument::OPTIONAL, 'The timer to delete');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $timers = timers();
        if (empty($timers)) {
            $output->write("<error>No timers defined.</error>");
            return;
        }

        /** @var QuestionHelper $q */
        $q = $this->getHelper('question');

        if ($input->hasArgument('timer') && array_key_exists($input->getArgument('timer'), $timers)) {
            $timer_id = $input->getArgument('timer');
        } elseif (count($timers) > 1) {
            $timer_id = $q->ask(
                $input,
                $output,
                new ChoiceQuestion(
                    "Which timer would you like to delete?",
                    array_map(
                        function (array $timer): string {
                            return $timer['description'];
                        },
                        $timers
                    )
                )
            );
        } elseif (count($timers) === 1) {
            $timer_id = key($timers);
        } else {
            $output->writeln("<error>Cannot determine timer to select.</error>");
            return;
        }

        $confirm = $q->ask(
            $input,
            $output,
            new ConfirmationQuestion("Confirm you wish to delete timer #{$timer_id} (y/N)", false)
        );

        if (!$confirm) {
            $output->writeln("<info>No action will be taken.</info>");
            return;
        }

        unset($timers[$timer_id]);

        timers($timers);
    }
}
