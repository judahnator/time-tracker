<?php

namespace judahnator\TimeTracker;

use judahnator\Option\Drivers\JsonFileDriver;
use judahnator\Option\Option;

if (!function_exists('judahnator\TimeTracker\log_location')) {
    /**
     * Returns the log location.
     *
     * @param string|null $setLocation
     * @return string
     */
    function log_location(string $setLocation = null): string
    {
        if (!is_null($setLocation)) {
            option()->set('log_location', $setLocation);
        }
        return option()->get('log_location', '');
    }
}

if (!function_exists('judahnator\TimeTracker\option')) {
    /**
     * Returns the static Option instance.
     *
     * @return Option
     */
    function option(): Option
    {
        static $option = null;
        if (is_null($option)) {
            $option = new Option(new JsonFileDriver(__DIR__.'/../options.json'));
        }
        return $option;
    }
}

if (!function_exists('judahnator\TimeTracker\timers')) {
    /**
     * Returns an array of the active timers.
     * Optionally updates the timers as well.
     *
     * @param array|null $setValue
     * @return array
     */
    function timers(array $setValue = null): array
    {
        if (!is_null($setValue)) {
            option()->set('timers', $setValue);
        }
        return option()->get('timers', []);
    }
}
