PHP Time Tracker
================

[![pipeline status](https://gitlab.com/judahnator/time-tracker/badges/master/pipeline.svg)](https://gitlab.com/judahnator/time-tracker/commits/master)

This is a super simple time tracking application built using the symfony console component.

I needed a time tracker, but did not really need a full web interface or a GUI to do something as simple as adding time logs to a CSV file. Not seeing a whole lot of options out in the wild, I decided to make my own.

Installation
------------

You can install this project by running `composer global require judahnator/time-tracker`. That will install the package globally, and if your composer bin directory is added to your system `$PATH` variable you can use the `timetracker` command.

Next you will need to set your time log location. The log file will be formatted as CSV.

`timetracker set:log '/full/path/to/your/spreadsheet.csv`

Usage
-----

You can find available commands by simply typing `timetracker`.

For a somewhat real-life example, say you wanted to start a timer for a client 'Trees Are Cool LLC'.

`timetracker timer:start -c 'Trees Are Cool LLC' 'Doing some pretty rad work on the website'`

You can see the active timer by using the list command.

`timetracker timer:list`

Say you wanted to edit the timer. You can use the `timer:edit` command interactively, or you can do it all with command options.

`timetracker timer:edit 5b16b3cb74d7c -c='Trees Are Cool LLC' -d='Fixing CSS bug'`

You then finish your work and want to add the time to the log. You use the `timer:stop` command here, it will help you pick which timer you want to stop and add to the log.

`timetracker timer:stop`

If you have a timer you wish to discard, you can use the `timetracker timer:delete` command.

Upgrade Help
------------

When upgrading this package it is important to note that the internal cache file will be overwritten. This is not the end of the world since as of the time of writing this it only holds the log location and active timers.

If you have installed this project standalone, there are before and after update scripts to copy the internal cache file to your systems temporary file directory and back. *These scripts will not run if the project has been installed globally via composer*

If you don't want to lose your active timers if you have installed the package globally, you should add two scripts to your global `composer.json` file. Its specific location will vary from system to system, but on my personal rig it is located at `~/.config/composer/composer.json`

The two scripts you need to add can be seen in this projects composer.json, just add them to your global `composer.json` and you should be golden. The file should look something like this:

```json
{
    "require": {
        // other packages ...
        "judahnator/time-tracker": "^1.0"
    },
    "scripts": {
        "pre-update-cmd": "judahnator\\TimeTracker\\Update::preUpdate",
        "post-update-cmd": "judahnator\\TimeTracker\\Update::postUpdate"
    }
}
```
 
